var axios = require('axios');

module.exports = function getNews(section='technology') {
    // CHAVE DA API DO NY TIMES
    const API_KEY = 'HVQAPBKTavmQRVaTTqWcZyQO3GM40YA4';

    // Nova instancia axios com a URL base
    const api = axios.create({
        baseURL: 'https://api.nytimes.com/svc/topstories/v2/',
    });

    const delay = interval => new Promise(resolve => setTimeout(resolve, interval));

    //Request
    const result = api.get(`${section}.json?api-key=${API_KEY}`)
        .then(async response => {
            await delay(1000); //60 por minuto
            return response.data;
        })
        .catch(async error => {
            await delay(1000);
            throw error.response;
        })
    return result;
};