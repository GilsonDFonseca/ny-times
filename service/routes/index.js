const express = require('express');
const router = express();
const getNews = require('../functions/getNews');

router.use('/', async (req, res) => {
    var news;
    try {
        news = await getNews(req.query[0]);
        res.send(news);
    }
    catch (error) {
        console.log(error)
        if (!error.status) {
            res.status('500').send(error.message);
        }
        res.status(error.status).send(error.data);
    }
})

module.exports = router;
