## Sobre
A aplicação será um portal utilizando React para a exibição de notícias e o Node.JS para o consumo das notícias de dois temas ('technology' e 'science') que estão dentro da api do THE NEW YORK TIMES.

## Instalação

Para instalar, rode na pasta raiz:

```npm
npm install
```

## Iniciar

Após instalar, para começar rode, também na raiz:

```npm
npm start
```

## Pontos Interessantes
* Divisão do projeto
	* O projeto foi divido em client e service. Para que não fosse necessário ter que ir em cada um das pastas para fazer instalação e inicialização, adicionei um script na pasta raiz que, ao receber os comandos, executa em ambas as pastas anteriormente mencionadas 
* Front-end
	* Foi utilizado Reactjs com material-ui, react-router e redux
	* Foi criada uma requisição base para contato com o back-end, e as requisições foram separadas em services para diminuir os impactos de possíveis erros que possam ocorrer
	* O redux é quem chama os services, dessa forma é possível manter os estados do que for necessário
	* O Material-ui é um framework com alguns componentes já previamente montados, para aceleração do desenvolvimento
	* O app foi separado em abas, onde a primeira mostra infos a respeito de tecnologia e a segunda ciências, a aba informa qual o tema a ser enviado para o back-end
	* A view de tecnologia foi montada a partir de um componente de lista a segunda foi montada para assimilar-se com a primeira, porém fazendo de maneira mais manual
	* A navegação ficou por conta do react-router
* Back-end
	* Separado em raiz, rotas e funções
	* A raiz adiciona alguns conroles de acesso, para minimizar problemas com as requisições
	* A rota recebe as funções e retorna a respostas destas
	* A função acessa a API do NYTIMES e retorna para a rota o resultado de tal requisição
	* Em função também existe um delay para que não sejam enviadas várias requisições, e para não ocorrer erro caso não seja recebido nenhum tema, foi adicionado 'technology' como section padrão

## Pirâmide de testes
Em uma pirâmide de testes existem 3 testes que são:

* Se pensarmos em uma pirâmide, o teste base seria o teste unitário, onde as menores porções do código são testadas


* Posteriormente, acima da base, existem os testes de integração onde essas unidades são testadas durante a interação das mesmas


* Por fim, no topo da pirâmide, os testes de ponta a ponta, onde comportamentos do usuário final são reproduzidos buscando encontrar erro na aplicação 
## License
[MIT](https://choosealicense.com/licenses/mit/)