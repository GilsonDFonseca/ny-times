import React from 'react';
import { Dialog, DialogTitle, DialogContent, Typography, Link, DialogActions, Button } from '@material-ui/core';

export default function DialogView(props) {
    const { data, isOpen, handleChange } = props;

    if (Object.keys(data).length === 0) {
        return (
            null
        )
    }

    return (
        <Dialog onClose={handleChange} open={isOpen} >
            <DialogTitle>
                <Typography variant="h5" gutterBottom component="h4">
                    {data.title}
                </Typography>
            </DialogTitle>
            <DialogContent dividers>
                <Typography variant="h6" gutterBottom component="h4">
                    {data.abstract}
                </Typography>
                <Typography variant="body1" component="h4">
                    {'read more'}
                    <Link href={data.short_url} target="_blank" >
                        {' ' + data.short_url}
                    </Link>
                </Typography>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleChange} variant="outlined" color="primary">
                    {'Fechar'}
                </Button>
            </DialogActions>
        </Dialog>
    );
}
