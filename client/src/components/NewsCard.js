import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography, CardMedia } from '@material-ui/core';
import Dialog from './Dialog';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflowY: 'auto',
        overflowX: 'hidden',
        backgroundColor: theme.palette.background.paper,
        height: '94vh'
    },
    grid: {
        position: 'relative',
    },

    titleBar: {
        padding: 16,
        width: '100%',
        background:
            'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
            'rgba(0,0,0,0.3) 80%, rgba(0,0,0,0) 100%)',
        color: theme.palette.background.paper,
    },

    media: {
        height: 0,
        paddingTop: '56.25%'
    },
}));

export default function RecipeReviewCard(props) {
    const { news } = props;
    const classes = useStyles();
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    const [openDialog, setOpenDialog] = useState(false);
    const [data, setData] = useState({});

    function handleClick(item) {
        setOpenDialog(true);
        setData(item);
    }

    return (
        <div className={classes.root}>
            <Grid
                container
                direction="row"
                justify="space-around"
                alignItems="center"
                spacing={1}
            >
                {news.map((item) => {
                    let imageInfo = item.multimedia[0];
                    return (
                        <Grid key={imageInfo.url + item.title} item xs={12} sm={12} className={classes.grid} onClick={() => handleClick(item)} >
                            <div className={classes.titleBar}>
                                <Typography variant="body1" component="h4">
                                    {item.title}
                                </Typography>
                                <Typography variant="caption" component="h4">
                                    {new Date(item.updated_date).toLocaleDateString(undefined, options)}
                                </Typography>
                            </div>
                            <CardMedia
                                className={classes.media}
                                image={imageInfo.url}
                                title={imageInfo.caption}
                            />
                        </Grid>
                    )
                })}
            </Grid>
            <Dialog data={data} isOpen={openDialog} handleChange={() => setOpenDialog(false)} />
        </div>
    );
}