import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Typography } from '@material-ui/core';


const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: '90vh'
    },
}));

export default function Error(props) {
    const { error } = props;
    const classes = useStyles();
    return (
        <Container maxWidth="md">
            <div component="div" className={classes.root}>
                <Typography variant="h5" component="h4">
                    {'OCORREU UM ERRO'}
                </Typography>
                <Typography variant="h6" color="secondary" component="h4">
                    {error.message}
                </Typography>
            </div>
        </Container>
    );
}