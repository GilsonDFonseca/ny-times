import React from 'react';
import { Container, AppBar, Tabs, Tab, Box } from '@material-ui/core';
import Error from '../Error';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box>
                    {children}
                </Box>
            )}
        </div>
    );
}

function EmptyMessage() {
    return (
        <Error error={{ message: 'Erro não identificado', status: 500 }} />

    )
}

export default function ContainerLayout(props) {
    const { disabled } = props;
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    let children = props.children && React.Children.map(props.children, child => {
        return child
    })

    if (!children) {
        return (
            <Container maxWidth="md">
                <AppBar position="static">
                    <Tabs value={value} onChange={handleChange} centered >
                        <Tab label="Tecnologia" disabled={disabled} />
                        <Tab label="Ciência" disabled={disabled}  />
                    </Tabs>
                </AppBar>
                <TabPanel value={value} index={0}>
                    {<EmptyMessage />}
                </TabPanel>
                <TabPanel value={value} index={1}>
                    {<EmptyMessage />}
                </TabPanel>
            </Container>
        )
    }
    return (
        <Container maxWidth="md">
            <AppBar position="static" style={{ marginBottom: 8 }}>
                <Tabs value={value} onChange={handleChange} centered >
                    <Tab label="Tecnologia" disabled={disabled}  />
                    <Tab label="Ciência" disabled={disabled}  />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                {children[value]}
            </TabPanel>
            <TabPanel value={value} index={1}>
                {children[value]}
            </TabPanel>
        </Container>
    );
}