import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';


const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: '90vh'
    },
}));

export default function Loading() {
    const classes = useStyles();

    return (
        <Container maxWidth="md">
            <div component="div" className={classes.root}>
                <CircularProgress />
            </div>
        </Container>
    );
}