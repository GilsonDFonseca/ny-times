import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Dialog from './Dialog';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
        height: '94vh'
    },
    gridList: {
        height: '100%',
        transform: 'translateZ(0)',
    },
    titleBar: {
        background:
            'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
            'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
}));

export default function GridListLayout(props) {
    const { news } = props;
    const classes = useStyles();
    const [openDialog, setOpenDialog] = useState(false);
    const [data, setData] = useState({});

    function handleClick(e, item) {
        setOpenDialog(true);
        setData(item);
    }

    return (
        <div className={classes.root}>
            <GridList spacing={2} className={classes.gridList}>
                {news.map((item) => {
                    let imageInfo = item.multimedia[0];
                    return (
                        <GridListTile key={imageInfo.url} cols={2} rows={3}  onClick={(e) => handleClick(e, item)}>
                            <img src={imageInfo.url} alt={imageInfo.caption} />
                            <GridListTileBar
                                title={item.title}
                                titlePosition="top"
                                className={classes.titleBar}
                            />
                        </GridListTile>
                    )
                })}
            </GridList>
            <Dialog data={data} isOpen={openDialog} handleChange={() => setOpenDialog(false)} />
        </div>
    );
}