import { newsService } from '../../service/news.service';

// CONST
export const Types = {
    GETNEWS: 'newsService/GETNEWS',
    LOADING: 'newsService/LOADING',
    ERROR: 'newsService/ERROR',
};

// INITIAL STATE

const initialState = {
    news: {},
    loading: false,
    error: {
        error: false,
        message: '',
        status: null,
    },
}

// REDUCER
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case Types.GETNEWS:
            return {
                ...state,
                news: {...state.news, ...action.payload.news}
            }
        case Types.LOADING:
            return {
                ...state,
                loading: action.payload.loading,
            }
        case Types.ERROR:
            return {
                ...state,
                loading: action.payload.loading,
                error: action.payload.error,
            }
        default:
            return state;
    }
}


// ACTIONS
function getNews(params) {
    return dispatch => {
        return newsService.getNews(params).then(data => {
            dispatch({
                type: Types.LOADING,
                payload: {
                    loading: true
                }
            });
            dispatch({
                type: Types.GETNEWS,
                payload: {
                    news: {[params]: data}
                }
            });
        }).catch(err => {
            dispatch({
                type: Types.ERROR,
                payload: {
                    error: {
                        error: true,
                        message: err.response.data,
                        status: err.response.status,
                    },
                    loading: false
                }
            });
        }).finally(
            dispatch({
                type: Types.LOADING,
                payload: {
                    loading: false
                }
            })
        )
    }
}

export const actions = {
    getNews
}
