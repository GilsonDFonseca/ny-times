import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { actions as newsActions } from '../redux/ducks/news';
import ContainerLayout from '../components/layouts/ContainerLayout';
import GridList from '../components/GridList';
import NewsCard from '../components/NewsCard';
import Loading from '../components/Loading';
import Error from '../components/Error';


export default function App() {
  const dispatch = useDispatch();
  const { news, loading, error } = useSelector(state => state.news);

  useEffect(() => {
    const params = ['technology', 'science'];
    if (!loading) {
      params.forEach(param => {
        dispatch(newsActions.getNews(param));
      })
    }
  }, [dispatch, loading]);

  if (error.error) {
    return (
      <ContainerLayout disabled={true}>
        <Error error = {error}/>
      </ContainerLayout >
    )
  }

  if (!error.error && (!news || (news && (!news.technology || !news.science)))) {
    return (
      <ContainerLayout disabled={true}>
        <Loading />
      </ContainerLayout >
    )
  }

  return (
    <ContainerLayout>
      <GridList news={news.technology.results} />
      <NewsCard news={news.science.results} />
    </ContainerLayout>
  );
}