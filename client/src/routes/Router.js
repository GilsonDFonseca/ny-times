import React from "react";
import App from '../scenes/App';
// import Recipe from '../scenes/Recipe';

import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

export default function Routers () {
    return (
        <Router>
            <Switch>
                <Route path="/" exact={true} component={App} />
                {/* <Route path="/recipe" exact={true} component={Recipe} /> */}
            </Switch>
        </Router>
    );
}