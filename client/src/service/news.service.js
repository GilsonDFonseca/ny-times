import { qrService } from './base.service';

function getNews (params) {
    return qrService(params)
}

export const newsService = {
    getNews,
}